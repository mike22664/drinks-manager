#!/usr/bin/env bash


function show_dm_help { # $1 = exit code

    echo -e "Usage:\t./run.sh <command>\n"
    echo -e "\nCommands:\n"
    echo -e "  server\t\tstart server"
    echo -e "  setup\t\t\tset up the application"
    echo -e "  create-admin\t\tcreate an admin account"
    echo -e "  generate-tls-cert\tgenerate a new self-signed tls certificate for https"
    echo -e "  generate-secret-key\tgenerate a new random secret key for django"
    echo -e "  clear-sessions\tmanually remove all expired sessions from the database"
    echo -e "  force-db-upgrade\tforce a database migration & upgrade"
    echo -e "  archive-tables\tarchive (copy & delete) all entries in app_order and app_registertransaction"
    echo -e "  development-server\tstart django development server and enable debugging"
    echo -e "  help\t\t\tShow this help text\n"
    echo -e "\nExamples:\n"
    echo -e "  ./run.sh server"
    echo -e "  ./run.sh create-admin"
    echo ""

    exit $1

}

# set current working directory
cd $(dirname "$0")

source "$(pwd)/lib/env.sh"

echo -e "\n## Drinks Manager"
echo -e "## version $APP_VERSION\n"


if [ -z $1 ]; then

    show_dm_help 1

else

    source "$(pwd)/config/config.sh"

    if [ $1 = 'server' ]; then

        source "$(pwd)/lib/auto-upgrade-db.sh"
        python3 "$(pwd)/lib/bootstrap.py"
    
    elif [ $1 = 'development-server' ]; then

        source "$(pwd)/lib/auto-upgrade-db.sh"
        python3 "$(pwd)/lib/bootstrap.py" devel
    
    elif [ $1 = 'setup' ]; then

        source "$(pwd)/lib/setup-application.sh"
    
    elif [ $1 = 'generate-tls-cert' ]; then

        source "$(pwd)/config/tls/cert-config.sh"
        python3 "$(pwd)/lib/generate-tls-cert.py"

    elif [ $1 = 'generate-secret-key' ]; then

        python3 "$(pwd)/lib/generate-secret-key.py" --override

    elif [ $1 = 'force-db-upgrade' ]; then

        source "$(pwd)/lib/db-migrations.sh"
        python3 "$(pwd)/lib/upgrade-db.py"

    elif [ $1 = 'create-admin' ]; then

        source "$(pwd)/lib/create-admin.sh"
    
    elif [ $1 = 'clear-sessions' ]; then

        source "$(pwd)/lib/clear-expired-sessions.sh"
        echo -e "done."
    
    elif [ $1 = 'archive-tables' ]; then

        python3 "$(pwd)/lib/archive-tables.py"

    elif [ $1 = 'help' ]; then

        show_dm_help 0

    else

        show_dm_help 1

    fi

fi