# Drinks Manager

Can't keep track of the number of drinks your guests drink?
Now you have a web interface that *really tries* to make things less complicated- for
you and your guests.  

This (exaggeration intended) most incredible piece of software is written in Python,
HTML, CSS, JS, Bash and uses Django and PostgreSQL.  
You have to bring your own PostgreSQL Database though.


## Setup, Installation, Updating and Dependencies

see [Setup](docs/Setup.md)


## Configuration

see [Configuration](docs/Configuration.md)


## Usage

After setup, run ```./run.sh help``` to see a help text.  

Start the production server with ```./run.sh server```. You can ignore the error message about the "lifespan error".

For more commands, see [Commands](docs/Commands.md).


## Versions

You can find the latest releases [here](https://gitlab.com/W13R/drinks-manager/-/releases). For Installation/Updating, you should consider using git, though (for more information see [Setup](docs/Setup.md)).

The releases are versioned after the following scheme:

`MAJOR`.`MINOR`

- `MAJOR`: will include changes  
   -> may be incompatible with the previous version
- `MINOR`: will only include bugfixes and smaller changes  
   -> may not be incompatible with the previous version
