# Commands

You run a command with

```
./run.sh <command>
```

## Available Commands

---

`server` - Start the server  
This starts a caddy instance, hypercorn with the django application and a scheduler that automatically removes expired session data.  
Log files will be written.

---

`setup` - Set up the application  
This sets up some database tables, views, and more, generates a secret key for the application and lets you create an admin user.

---

`create-admin` - Lets you create an admin user

---

`generate-tls-cert` - generate a new self-signed tls certificate for https  
This overwrites the original files, if present (see [Setup](Setup.md)).

---

`generate-secret-key` - generate a new random secret key for django  
This will overwrite the old one.  
Warning: After running this, current sessions will be invalid, and the users have to relogin. Don't run this command while the server is running.

---

`clear-sessions` - manually remove all expired sessions from the database

---

`force-db-upgrade` - force a database migration and -upgrade
This is mainly used in development.

---

`archive-tables` - archive (copy & delete) all entries in app_order and app_registertransaction
Use this to archive old orders or transactions (e.g. when the database gets too big).

---

`development-server` - Start the development server  
This starts a caddy instance, the django development server with DEBUGGING enabled and the session-clear-scheduler.  
Only the HTTP-Access-Log will be written to its logfile, other logs will be written to the console.

---

`help` - Show a help text

---


## Examples

Run the production server:
```
./run.sh server
```

Create a new admin:
```
./run.sh create-admin
```