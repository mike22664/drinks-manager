# Setup

## I. Dependencies

Before the actual setup, you have to satisfy the following dependencies:


### System

- `pg_config`
    - Fedora/RHEL: `libpq-dev`
- `Caddy` 2.4.3+ (HTTP Reverse Proxy & Static File Server)
- `gcc`, `gettext`
- `Python` 3.9 with pip
    - `Python` header files
        - Fedora/RHEL: `python3-devel`


### Python Packages (pip)

- `django~=3.2.7`
- `django-currentuser==0.5.3`
- `django-csp==3.7`
- `psycopg2~=2.9.1`
- `hypercorn~=0.11.2`
- `cryptography~=36.0.0` (for self-signed tls certificates)

You can install those pip-packages with the following command:  
```bash
pip install -U -r pip-dependencies.txt
```

## II.A Installation

You can get the latest version with git:

```
git clone --branch release-x.x https://gitlab.com/W13R/drinks-manager.git
```
(replace x.x with the latest version)

Alternatively, you can download the [latest release](https://gitlab.com/W13R/drinks-manager/-/releases) and extract the files to your prefered destination.  

<u>**Warning:**</u>

Make shure that you set the correct file permissions, especially for the config files !!

The following should be sufficient:

```bash
chmod -R u+rw,g+r,g-w,o-rwx <drinks_manager_directory>
```


## II.B Update 

If you installed the application with git, you can run the following in the drinks-manager directory to update to the new version:

```
git fetch
git checkout release-x.x
```
(replace x.x with the new version)

If you downloaded the application from the releases page, you can download the new release in the same manner, and overwrite the old files with the new ones.

You have to restart the application server to apply the changes.  
WARNING: The auto-upgrade mechanism may expect you to input information. Therefore, you should start the application from the command-line the first time after an update.  

Further upgrading-instructions may be provided in the Release Notes on the Releases Page of this Project (Deployments -> Releases).


## III. Database

This project is using PostgreSQL. You have to set up a database by yourself.  
The database must have the schema `public` (exists on a new database). Make shure that you create a database user with the necessary privileges to write to and read from the database (SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, CREATE, CONNECT):

```sql
-- connected to target database
grant SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES on all tables in schema public to <dbuser>;
grant CREATE, CONNECT on database <dbname> to <dbuser>;
```

You can configure your database connection in `config/config.sh`.


## IV. HTTPS & TLS Certificates

TLS/SSL certificates are required.
If you don't have a TLS/SSL certificate already, you can generate one
with the command `./run.sh generate-tls-cert`. This will generate a 
new TLS certificate and key file at `config/tls/server.pem` (certificate)
and `config/tls/server-key.pem` (key).  
WARNING: This will overwrite an existing certificate/key with the same filepath.
By default those generated certificates are valid for one year. After that year,
they have to be regenerated with the same command.  

If you have a certificate and key file already, you can put them in the following places:

- `config/tls/server.pem` for the certificate 
- `config/tls/server-key.pem` for the key

You can set another filepath for those files in your caddy configuration at `config/Caddyfile`.


## V. Configuration

see [Configuration](Configuration.md)


## VI. Run Setup Command

run `./run.sh setup`  

This will automatically set up database tables, views and entries, set up Django and let you create a admin user.

After this, start the server with `./run.sh server` and navigate to `https://your.ip.add.ress:port/admin/`.
