# Configuration

## Main Configuration  

`./config/config.sh`  

There is no default configuration available, only a sample configuration with explanations.


## Configuration files for tls certificates

This is the configuration for self-signed local TLS certificate generation.  

`./config/tls/cert-config.sh`

This is already configured, but you can modify this for your needs.


## Caddy Server Configuration  

`./config/Caddyfile`  

The default configuration should work out of the box, don't edit this file unless you know what you're doing.
