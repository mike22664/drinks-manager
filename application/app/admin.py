#

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.views.decorators.cache import never_cache

from .models import User
from .models import Drink
from .models import Order
from .models import Global
from .models import RegisterTransaction as Register

from .forms import CustomUserChangeForm
from .forms import CustomDrinkForm
from .forms import CustomGlobalForm
from .forms import CustomRegisterTransactionForm

# Admin Site

class CustomAdminSite(admin.AdminSite):

    site_header = "Drinks Administration"
    site_title = "Drinks Administration"
    
    @never_cache
    def index(self, request, extra_context=None):

        return super().index(request, extra_context={
            "registerBalance": "{:10.2f}".format(
                Global.objects.get(name="register_balance").value_float
            ),
            "admin_info": Global.objects.get(name="admin_info").value_string,
            **(extra_context or {})
        })

adminSite = CustomAdminSite()


# Register your models here.

class CustomUserAdmin(UserAdmin):

    model = User
    form = CustomUserChangeForm

    fieldsets_ = list((*UserAdmin.fieldsets,))
    fieldsets_.insert(1, (
            "Balance",
            {"fields": ("balance",)},
        )
    )
    fieldsets = tuple(fieldsets_)

    list_display = ["username", "balance", "is_active"]

    def get_actions(self, request): # remove the "delete_selected" action because it breaks some functionality
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

adminSite.register(User, CustomUserAdmin)


class CustomDrinkAdmin(admin.ModelAdmin):

    model = Drink
    form = CustomDrinkForm
    list_display = ["product_name", "content_litres", "price", "available", "binary_availability", "deleted"]

adminSite.register(Drink, CustomDrinkAdmin)


class CustomRegisterAdmin(admin.ModelAdmin):

    model = Register
    form = CustomRegisterTransactionForm
    site_title = "Register"
    list_display = ["datetime", "transaction_sum", "user", "comment"]
    actions = ["delete_selected_new"]

    def get_actions(self, request): # remove the "delete_selected" action because it breaks some functionality
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions
    
    def delete_selected_new(self, request, queryset):
        #print(queryset)
        for supply in queryset:
            #print(order)
            supply.delete()
        if queryset.count() < 2:
            self.message_user(request, f"Revoked {queryset.count()} supply.")
        else:
            self.message_user(request, f"Revoked {queryset.count()} supplies.")
    delete_selected_new.short_description = "Revoke selected transactions"


adminSite.register(Register, CustomRegisterAdmin)


class CustomOrderAdmin(admin.ModelAdmin):

    model = Order
    list_display = ["product_name", "amount", "price_sum", "user", "datetime"]
    actions = ["delete_selected_new"]

    def get_actions(self, request): # remove the "delete_selected" action because it breaks some functionality
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def delete_selected_new(self, request, queryset):
        #print(queryset)
        for order in queryset:
            #print(order)
            order.delete()
        self.message_user(request, f"Revoked {queryset.count()} order(s).")
    delete_selected_new.short_description = "Revoke selected orders"
            
adminSite.register(Order, CustomOrderAdmin)


class CustomGlobalAdmin(admin.ModelAdmin):

    model = Global
    form = CustomGlobalForm
    list_display = ["name", "value_float", "value_string"]

    def get_actions(self, request): # remove the "delete_selected" action because it breaks some functionality
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

adminSite.register(Global, CustomGlobalAdmin)
