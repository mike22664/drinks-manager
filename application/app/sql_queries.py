#from datetime import datetime

from django.conf import settings
from django.db import connection


def _select_from_db(sql_select:str):
    result = None
    with connection.cursor() as cursor:
        cursor.execute(sql_select)
        result = cursor.fetchall()
    return result


def select_history(user, language_code="en") -> list:
    # select order history and deposits
    user_id = user.pk
    result = _select_from_db(f"""
    select
        concat(
        product_name, ' (',
        content_litres::real, -- converting to real removes trailing zeros
        'l) x ', amount, ' - ', price_sum, '{settings.CURRENCY_SUFFIX}') as "text",
        datetime
    from app_order
    where user_id = {user_id}

    union 

    select
        concat('Deposit: +', transaction_sum, '{settings.CURRENCY_SUFFIX}') as "text",
        datetime
    from app_userdeposits_view
    where user_id = {user_id}

    order by datetime desc
    fetch first 30 rows only;
    """)
    result = [list(row) for row in result]
    if language_code == "de": # reformat for german translation
        for row in result:
            row[0] = row[0].replace(".", ",")
    return result


def select_yopml12m(user) -> list:
    # number of orders per month (last 12 months)
    # only for the specified user
    user_id = user.pk
    result = _select_from_db(f"""
        -- select the count of the orders per month (last 12 days)
        select
            to_char(date_trunc('month', datetime), 'YYYY-MM') as "month",
            sum(amount) as "count"
        from app_order
            where user_id = {user_id}
            and date_trunc('month', datetime) > date_trunc('month', now() - '12 months'::interval)
        group by "month"
        order by "month" desc;
    """)
    return [list(row) for row in result]

def select_aopml12m() -> list:
    # number of orders per month (last 12 months)
    result = _select_from_db(f"""
        -- select the count of the orders per month (last 12 days)
        select
            to_char(date_trunc('month', datetime), 'YYYY-MM') as "month",
            sum(amount) as "count"
        from app_order
            where date_trunc('month', datetime) > date_trunc('month', now() - '12 months'::interval)
        group by "month"
        order by "month" desc;
    """)
    return [list(row) for row in result]


def select_yopwd(user) -> list:
    # number of orders per weekday (all time)
    # only for the specified user
    user_id = user.pk
    result = _select_from_db(f"""
        -- select the count of the orders per weekday (all time)
        select
            to_char(datetime, 'Day') as "day",
            sum(amount) as "count"
        from app_order
            where user_id = {user_id}
        group by "day"
        order by "count" desc;
    """)
    return [list(row) for row in result]
    return []

def select_aopwd() -> list:
    # number of orders per weekday (all time)
    result = _select_from_db(f"""
        -- select the count of the orders per weekday (all time)
        select
            to_char(datetime, 'Day') as "day",
            sum(amount) as "count"
        from app_order
        group by "day"
        order by "count" desc;
    """)
    return [list(row) for row in result]
    return []


def select_noyopd(user) -> list:
    # number of orders per drink (all time)
    # only for specified user
    user_id = user.pk
    result = _select_from_db(f"""
        select
            d.product_name as "label",
            sum(o.amount) as "data"
        from app_drink d
            join app_order o on (d.id = o.drink_id)
        where o.user_id = {user_id}
        group by d.product_name
        order by "data" desc;
    """)
    return [list(row) for row in result]

def select_noaopd() -> list:
    # number of orders per drink (all time)
    result = _select_from_db(f"""
        select
            d.product_name as "label",
            sum(o.amount) as "data"
        from app_drink d
            join app_order o on (d.id = o.drink_id)
        group by d.product_name
        order by "data" desc;
    """)
    return [list(row) for row in result]