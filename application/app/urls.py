from django.urls import path, include
from django.contrib.auth import views as auth_views

from . import views
from .admin import adminSite

urlpatterns = [
    path('', views.index),
    path('order/<drinkID>/', views.order),
    path('history/', views.history),
    path('deposit/', views.deposit),
    path('statistics/', views.statistics),
    path('accounts/login/', views.login_page, name="login"),
    path('accounts/logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('accounts/password_change/', auth_views.PasswordChangeView.as_view(), name='password_change'),
    path('accounts/password_change_done/', views.redirect_home, name='password_change_done'),
    path('admin/', adminSite.urls),
    # API #
    path('api/order-drink', views.api_order_drink),
    path('api/deposit', views.api_deposit),
    #path('api/get-statistics', views.api_get_statistics)
]