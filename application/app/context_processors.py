from django.conf import settings

from .models import Global

def app_version(request):

    try:
        global_message = Global.objects.get(pk="global_message").value_string
    except Global.DoesNotExist:
        global_message = ""
    
    return {
        "app_version": settings.APP_VERSION,
        "currency_suffix": settings.CURRENCY_SUFFIX,
        "global_message": global_message
    }
