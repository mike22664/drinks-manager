import json
import sys

from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.http.response import HttpResponseRedirect
from django.http.response import HttpResponse
from django.shortcuts import render

from django.utils.translation import gettext as _

from django.utils.formats import decimal

from . import sql_queries

from .models import Drink
from .models import Order
from .models import RegisterTransaction


# login view


def login_page(request):

    next_in_url = request.GET.get('next', '/')

    userlist = get_user_model().objects.filter(is_superuser=False).filter(is_active=True)

    if request.method == "POST":

        form = AuthenticationForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        next_ = request.POST['next']

        user = authenticate(username=username,password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(next_)
        else:
            return render(request,'registration/login.html', {
                "form": form,
                "next": next_in_url,
                "user_list": userlist,
                "error_message": _("Invalid username or password.")
            })
         
    else:
        
        if request.user.is_authenticated:
            return HttpResponseRedirect(next_in_url)
        
        form = AuthenticationForm()

    return render(request,'registration/login.html', {
        "form": form,
        "next": next_in_url,
        "user_list": userlist
    })



# actual application

@login_required
def index(request):
    context = {
        "available_drinks": Drink.objects.filter(available__gt=0).filter(deleted=False),
    }
    return render(request, "index.html", context)

@login_required
def history(request):
    context = {
        "history": sql_queries.select_history(request.user, language_code=request.LANGUAGE_CODE),
    }
    return render(request, "history.html", context)

@login_required
def order(request, drinkID):
    try:
        drink_ = Drink.objects.get(pk=drinkID)
        context = {
            "drink": drink_
        }
        return render(request, "order.html", context)
    except Drink.DoesNotExist:
        return HttpResponseRedirect("/")

@login_required
def deposit(request):
    return render(request, "deposit.html", {})


@login_required
def statistics(request):
    context = {
        "yopml12m": sql_queries.select_yopml12m(request.user),
        "aopml12m": sql_queries.select_aopml12m(),
        "yopwd": sql_queries.select_yopwd(request.user),
        "aopwd": sql_queries.select_aopwd(),
        "noyopd": sql_queries.select_noyopd(request.user),
        "noaopd": sql_queries.select_noaopd()
    }
    return render(request, "statistics.html", context)

@login_required
def redirect_home(request):
    return HttpResponseRedirect("/")


# API for XHR requests #

@login_required
def api_order_drink(request):

    # check request -> make order

    user = request.user

    try:

        drinkID = int(request.POST["drinkID"])
        amount = int(request.POST["numberOfDrinks"])

        drink = Drink.objects.get(pk=drinkID)

        if ((drink.binary_availability and drink.available > 0) or (drink.available >= amount)) and not drink.deleted:
            Order.objects.create(drink=drink, user=user, amount=amount)
            return HttpResponse("success", status=200)
        else:
            return HttpResponse("notAvailable", status=400)

    except Exception as e:
        print(f"User: {user.username} - Exception: {e}", file=sys.stderr)
        return HttpResponse(b"", status=500)


@login_required
def api_deposit(request):

    # check request -> deposit

    user = request.user

    try:

        amount = decimal.Decimal(request.POST["depositAmount"])

        if 0.00 < amount < 9999.99:
            # create transaction
            RegisterTransaction.objects.create(
                transaction_sum=amount,
                comment=f"User deposit by user {user.username}",
                is_user_deposit=True,
                user=user
            )
            #
            return HttpResponse("success", status=200)
        else: raise Exception("Deposit amount too big or small.")
    
    except Exception as e:
        print(f"User: {user.username} - Exception: {e}", file=sys.stderr)
        return HttpResponse(b"", status=500)