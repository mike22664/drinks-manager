
from django import forms
from django.conf import settings
from django.contrib.auth.forms import UserChangeForm

from .models import User
from .models import Drink
from .models import RegisterTransaction
from .models import Global


class CustomUserChangeForm(UserChangeForm):

    balance = forms.DecimalField(max_digits=8, decimal_places=2, initial=0.00, label=f"Balance {settings.CURRENCY_SUFFIX}")

    class Meta:
        model = User
        fields = ("username", "balance")


class CustomDrinkForm(forms.ModelForm):

    product_name = forms.CharField(max_length=64, label="Product Name")
    content_litres = forms.DecimalField(max_digits=6, decimal_places=3, initial=0.5, label="Content (l)")
    price = forms.DecimalField(max_digits=6, decimal_places=2, label=f"Price {settings.CURRENCY_SUFFIX}")

    class Meta:
        model = Drink
        fields = ("product_name", "content_litres", "price", "binary_availability", "available", "deleted")


class CustomRegisterTransactionForm(forms.ModelForm):

    class Meta:
        model = RegisterTransaction
        fields = ("transaction_sum", "datetime", "is_user_deposit", "comment", "user")


class CustomGlobalForm(forms.ModelForm):

    comment = forms.CharField(widget=forms.Textarea, required=False)
    value_float = forms.FloatField(initial=0.00)
    value_string = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = Global
        fields = ("name", "comment", "value_float", "value_string")
