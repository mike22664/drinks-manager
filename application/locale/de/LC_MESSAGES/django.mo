��    4      �  G   \      x     y  %   �     �     �  2   �  	                   (     8     ?  2   S     �     �     �     �     �     �     �     �     �               $     8     @     ^  +   m     �     �     �     �     �     �  
   �     �            &   &     M  	   e     o     t     {     �     �     �     �     �     �     �  B  �     �  .   	     F	     f	  :   l	  
   �	     �	     �	     �	  
   �	     �	  W   

     b
  	   i
     s
     �
     �
     �
     �
     �
     �
     �
          %     =  %   E     k  :   x     �  *   �     �  
                   +  ,   7     d     m  /   �      �  
   �     �  	   �     �                         /     8  	   >                   .      %          $   "       (          
                 3   !          ,         /   #   0      -                         )         *                    &       '         +                              4   1      2             	        All orders per drink All orders per month (last 12 months) All orders per weekday Amount An error occured. Please log out and log in again. Available Available Drinks Balance Change Password Choose Choose your account Click here if automatic redirection does not work. Count Deposit Django administration Django site admin Drink Drinks - Deposit Drinks - History Drinks - Home Drinks - Logged Out Drinks - Login Drinks - Order Drinks - Statistics History Invalid username or password. Log in as user Logged out! You will be redirected shortly. Logout No drinks available. No history. Order Password/PIN Price per Item Statistics This drink is not available. User Your orders per drink Your orders per month (last 12 months) Your orders per weekday available back cancel confirm count day drink last 30 actions login month order Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Alle Bestellungen pro Getränk Alle Bestellungen pro Monat (letzte 12 Monate) Alle Bestellungen pro Wochentag Summe Ein Fehler ist aufgetreten. Bitte ab- und wieder anmelden. Verfügbar Verfügbare Getränke Saldo Passwort ändern Auswählen Bitte wählen Sie Ihren Account Bitte klicken Sie hier, wenn die automatische Weiterleitung nicht funktionieren sollte. Anzahl Einzahlen Django Administration Django Administrator Getränk Getränke - Einzahlen Getränke - Verlauf Getränke - Home Getränke - Abgemeldet Getränke - Anmeldung Getränke - Bestellen Getränke - Statistiken Verlauf Benutzername oder Passwort ungültig. Anmelden als Sie wurden abgemeldet und werden in Kürze weitergeleitet. Abmelden Es sind gerade keine Getränke verfügbar. Kein Verlauf verfügbar. Bestellung Passwort/PIN Preis pro Getränk Statistiken Dieses Getränk ist gerade nicht verfügbar. Benutzer Deine Bestellungen pro Getränk Deine Bestellungen pro Monat (letzte 12 Monate) Deine Bestellungen pro Wochentag verfügbar zurück Abbrechen Bestätigen Anzahl Tag Getränk letzte 30 Vorgänge Anmelden Monat Bestellen 