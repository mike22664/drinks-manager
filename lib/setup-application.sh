#!/usr/bin/env bash


# enable debugging for this command
export DJANGO_DEBUG="true"

python3 "$(pwd)/lib/generate-secret-key.py"

source "$(pwd)/lib/db-migrations.sh"

python3 $(pwd)/lib/upgrade-db.py

echo -e "\nCreate admin account. Email is optional.\n"
source "$(pwd)/lib/create-admin.sh"

python3 $(pwd)/application/manage.py collectstatic --noinput
