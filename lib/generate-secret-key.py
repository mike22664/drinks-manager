#!/usr/bin/env python3

import sys

from pathlib import Path
from secrets import token_bytes
from base64 import b85encode

#

override = False
if len(sys.argv) > 1:
    if sys.argv[1] == "--override":
        override = True

random_token_length = 128

secret_key_fp = Path("config/secret_key.txt")

#

if secret_key_fp.exists() and not override:
    print(f"Warning: secret_key.txt already exists in directory {secret_key_fp.absolute()}. Won't override.", file=sys.stderr)
    exit(1)
else:
    print("Generating random secret key...")
    random_key = b85encode(token_bytes(random_token_length))
    with secret_key_fp.open("wb") as secret_key_f:
        secret_key_f.write(random_key)
    print("done.")