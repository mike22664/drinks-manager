#!/usr/bin/env python3


from os import environ
from os import getcwd
from shlex import quote
from signal import SIGINT
from subprocess import run
from subprocess import Popen
from sys import argv
from sys import stdout
from sys import stderr


# devel or prod?

devel = False

try:
    if argv[1] == "devel":
        devel = True
except IndexError:
    pass


# vars

pwd = getcwd()

APPLICATION_LOG = environ["APPLICATION_LOG"]
CADDY_ACCESS_LOG = environ["CADDY_ACCESS_LOG"]
CADDY_LOG = environ["CADDY_LOG"]

DJANGO_PORT = environ["DJANGO_PORT"]
HTTPS_PORT = environ["HTTPS_PORT"]

if devel:
    environ["DJANGO_DEBUG"] = "true"
else:
    environ["DJANGO_DEBUG"] = "false"


# info

print(f"\n\nStarting server on port {HTTPS_PORT}...\nYou should be able to access the application locally at https://localhost:{HTTPS_PORT}/\n\nPress Ctrl+C to stop all services.\n\n")
if not devel:
    print(f"All further messages will be written to {APPLICATION_LOG} and {CADDY_LOG}")
print(f"HTTP Access Log will be written to {CADDY_ACCESS_LOG}")


try:

    # start django/hypercorn

    if devel:
        run(
            ["python3", f"{pwd}/application/manage.py", "collectstatic", "--noinput"],
            stdout=stdout,
            stderr=stderr,
            env=environ
        )
        app_process = Popen(
            ["python3", f"{pwd}/application/manage.py", "runserver", f"localhost:{DJANGO_PORT}"],
            stdout=stdout,
            stderr=stderr,
            env=environ
        )
    else:
        application_log_file = open(APPLICATION_LOG, "a")
        run(
            ["python3", f"{pwd}/application/manage.py", "collectstatic", "--noinput"],
            stdout=application_log_file,
            stderr=application_log_file,
            env=environ
        )
        app_process = Popen(
            [
                "hypercorn", "--bind", quote(f"localhost:{DJANGO_PORT}"), "drinks_manager.asgi:application"
            ],
            stdout=application_log_file,
            stderr=application_log_file,
            cwd=f"{pwd}/application/",
            env=environ
        )

    # start caddy

    if devel:
        caddy_log_file = stdout
        caddy_log_file_stderr = stderr
    else:
        caddy_log_file = caddy_log_file_stderr = open(CADDY_LOG, "a")
    
    caddy_process = Popen(
        ["caddy", "run", "--config", f"{pwd}/config/Caddyfile"],
        stdout=caddy_log_file,
        stderr=caddy_log_file_stderr,
        env=environ
    )

    # start session-clear-scheduler

    if devel:
        clear_sched_log_file = stdout
        clear_sched_log_file_stderr = stderr
    else:
        clear_sched_log_file = clear_sched_log_file_stderr = open(APPLICATION_LOG, "a")
    
    scs_process = Popen(
        ["python3", f"{pwd}/lib/session-clear-scheduler.py"],
        stdout=clear_sched_log_file,
        stderr=clear_sched_log_file_stderr
    )

    caddy_process.wait()
    scs_process.wait()
    app_process.wait()


except KeyboardInterrupt:

    # exit

    print("\n\nStopping services.\n\n")

    caddy_process.send_signal(SIGINT)
    scs_process.send_signal(SIGINT)
    app_process.send_signal(SIGINT)

    caddy_process.wait()
    print(f"Caddy stopped with exit code {caddy_process.returncode}.")
    
    scs_process.wait()
    print(f"session-clear-scheduler stopped with exit code {scs_process.returncode}.")
    
    app_process.wait()
    if devel:
        print(f"Django stopped with exit code {app_process.returncode}.")
    else:
        print(f"Django/Hypercorn stopped with exit code {app_process.returncode}.")

    if caddy_process.returncode != 0 or scs_process.returncode != 0 or app_process.returncode !=0:
        exit(1)
    else:
        exit(0)
