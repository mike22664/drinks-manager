#!/usr/bin/env bash


# enable debugging for this command
export DJANGO_DEBUG="true"

# make migrations & migrate
python3 $(pwd)/application/manage.py createsuperuser

echo -e "done."