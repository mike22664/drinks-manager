#!/usr/bin/env bash


echo -e "Checking if database needs an upgrade..."

if python3 $(pwd)/lib/verify-db-app-version.py; then

    echo -e "No database upgrade needed."

else

    echo -e "Starting automatic database upgrade..."
    source "$(pwd)/lib/db-migrations.sh"
    python3 $(pwd)/lib/upgrade-db.py

fi
