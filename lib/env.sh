#!/usr/bin/env bash

export DJANGO_SK_ABS_FP="$(pwd)/config/secret_key.txt"
export STATIC_FILES="$(pwd)/static/"
export APP_VERSION="2.2"
