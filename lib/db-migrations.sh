#!/usr/bin/env bash


# enable debugging for this command
export DJANGO_DEBUG="true"

# make migrations & migrate
python3 $(pwd)/application/manage.py makemigrations
python3 $(pwd)/application/manage.py makemigrations app
python3 $(pwd)/application/manage.py migrate

echo -e "done with db migration."