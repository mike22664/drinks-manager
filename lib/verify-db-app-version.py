#!/usr/bin/env python3

from os import environ
from pathlib import Path

from psycopg2 import connect
from psycopg2._psycopg import cursor
from psycopg2 import Error
from psycopg2 import errorcodes


# verify if the installation
# exit code 0 -> no database update is necessary
# exit code 1 -> database update is necessary


def check_file():

    db_app_version_file = Path("./config/db_app_version.txt")

    if not db_app_version_file.exists():
        exit(1)
    if not db_app_version_file.is_file():
        exit(1)
    if not db_app_version_file.read_text().strip(" ").strip("\n") == environ["APP_VERSION"]:
        exit(1)


def check_database():

    try:

        connection = connect(
            user = environ["PGDB_USER"],
            password = environ["PGDB_PASSWORD"],
            host = environ["PGDB_HOST"],
            port = environ["PGDB_PORT"],
            database = environ["PGDB_DB"]
        )
        
        cur = connection.cursor()

        # check application version in db

        cur.execute("""
            select value from application_info
            where key = 'app_version';
        """)

        appinfo_result = list(cur.fetchone())[0]

        if appinfo_result == None:
            cur.close()
            connection.close()
            exit(1)
        
        if appinfo_result != environ["APP_VERSION"]:
            cur.close()
            connection.close()
            exit(1)
        
        # check rows in app_global

        required_rows = [
            "global_message",
            "register_balance",
            "admin_info"
        ]

        cur.execute("""
            select name from app_global;
        """)

        table_global_result = list(cur.fetchall())

        cur.close()
        connection.close()

        existing_rows = [list(row)[0] for row in table_global_result]
        
        for r in required_rows:
            if not r in existing_rows:
                exit(1)
    
    except Error:

        cur.close()
        connection.close()
        exit(1)
    
    except Exception as e:

        print(f"An exception occured: {e}")
        cur.close()
        connection.close()
        exit(1)
    


if __name__ == "__main__":

    check_file()
    check_database()
    
    exit(0)
