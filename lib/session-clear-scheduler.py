#!/usr/bin/env python3

# This script clears expired sessions in a regular interval
# The interval is defined (in minutes) by config.sh (SESSION_CLEAR_INTERVAL)

import os

from pathlib import Path
from subprocess import run
from time import sleep
from datetime import datetime

try:

    exiting = False
    clear_running = False

    print("[session-clear-scheduler] Starting session-clear-scheduler.")

    session_clear_script_fp = Path("lib/clear-expired-sessions.sh")
    clear_interval_seconds = int(os.environ["SESSION_CLEAR_INTERVAL"]) * 60

    sleep(10) # wait some seconds before the first session clean-up

    while True:

        clear_running = True
        run(["/bin/sh", session_clear_script_fp.absolute()])
        clear_running = False

        print(f"[session-clear-scheduler: {datetime.now()}] Cleared expired sessions.")

        if exiting:
            break

        sleep(clear_interval_seconds)

except KeyboardInterrupt:

    exiting = True
    
    if clear_running:
        print(f"[session-clear-scheduler: {datetime.now()}] Received SIGINT. Waiting for current clear process to finish.")
        sleep(20) # wait some time
    
    print(f"[session-clear-scheduler: {datetime.now()}] Exiting")
    exit(0)
