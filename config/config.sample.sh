# environment variables

export HTTP_PORT=80 # required by caddy, will be redirected to https
export HTTPS_PORT=443 # actual port for webinterface

export DJANGO_PORT=8001 # caddy's http port (should be blocked by the firewall)

export DJANGO_SESSION_COOKIE_AGE=600 # auto-logout, in seconds
export SESSION_CLEAR_INTERVAL=120 # interval for automatic session clearing, in minutes

export DJANGO_LANGUAGE_CODE="en" # the default and fallback language. Currently only de and en are supported.
export DJANGO_TIME_ZONE="CET"

export CURRENCY_SUFFIX="$" # if you have another currency symbol, you can specify it here

# Do you want to enable password validation? 
# (numeric PINs as Password will not be seen as valid)
export DJANGO_ENABLE_PASSWORD_VALIDATION="true"

# database connection (postgresql)
export PGDB_DB=""     # The name of the databae
export PGDB_USER=""     # The database user
export PGDB_PASSWORD='' # The password for the database user
export PGDB_HOST=""     # The hostname of your database (e.g. example.org or 127.0.0.1)
export PGDB_PORT=5432   # The port your database is listening on

# log files
# only change if you know what you are doing
export CADDY_ACCESS_LOG="$(pwd)/logs/http-access.log"
export CADDY_LOG="$(pwd)/logs/caddy.log"
export APPLICATION_LOG="$(pwd)/logs/application.log"
