# environment variables for tls generation

export TLS_EXPIRE_AFTER_DAYS=365
export TLS_COMMON_NAME="localhost"
export TLS_ALT_NAME1="127.0.0.1"
export TLS_ALT_NAME2="localhost.localdomain"
