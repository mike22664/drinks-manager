
def parse_config_from_file(filepath):

    config = {}

    with open(filepath, "r") as f:
        lines = f.readlines()
        for line in lines:
            line = line.lstrip(" ").replace("\n", "")
            if line.startswith("export "):
                line = line.replace("export ", "").lstrip(" ")
                varname = line[:line.find("=")]
                varvalue = line[line.find("=")+1:] 
                if varvalue.startswith("'"): varvalue = varvalue.strip("'")
                elif varvalue.startswith('"'): varvalue = varvalue.strip('"')
                config[varname] = varvalue
    
    return config
