#!/usr/bin/env python3

import os, sys

from pathlib import Path

from psycopg2 import connect
from psycopg2 import Error

from lib import parse_config_from_file


USER_ID = 2
N_NEW_ORDER_ROWS = 1000000
COMMIT_AFTER = 50
AMOUNT_PER_ORDER = 1
PRODUCT_NAME = "Wasser"
DRINK_ID = 1


if __name__ == "__main__":


    print("\nGetting config...")

    config_file = Path(Path(os.path.dirname(__file__)).parent / "config" / "config.sh").absolute()

    config = parse_config_from_file(config_file)

    print(f"Commit will be done after every {COMMIT_AFTER} rows.")

    x = input(f"Do you want to add {N_NEW_ORDER_ROWS} rows to the app_order table? (enter 'yes' to continue) ")
    try:
        if str(x) != "yes":
            exit()
    except ValueError:
        exit()

    try:

        print("\nConnecting to database...")

        conn = connect(
            user = config["PGDB_USER"],
            password = config["PGDB_PASSWORD"],
            host = config["PGDB_HOST"],
            port = config["PGDB_PORT"],
            database = config["PGDB_DB"]
        )

        cur = conn.cursor()

        for i in range(N_NEW_ORDER_ROWS):

            cur.execute(f"""
                insert into app_order (datetime, product_name, price_sum, content_litres, drink_id, user_id, amount)
                values (
                    current_timestamp,
                    '{PRODUCT_NAME}',
                    10.00,
                    0.5,
                    {DRINK_ID},
                    {USER_ID},
                    {AMOUNT_PER_ORDER}
                )
            """)

            if i % COMMIT_AFTER == 0 and not i == 0:
                conn.commit()
                print(f"\nAdded {i} rows")
        
        conn.commit()
        print(f"\nAdded {N_NEW_ORDER_ROWS} rows")

        print("done with db setup.")

    except (Error, Exception) as err:

        print(f"An error occured while connecting to the database {config['PGDB_DB']} at {config['PGDB_HOST']}:\n{err}", file=sys.stderr)

    finally:

        cur.close()
        conn.close()
