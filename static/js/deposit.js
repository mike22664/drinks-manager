document.addEventListener("DOMContentLoaded", () => {

    // elements

    let deposit_form = document.getElementById("depositForm");
    let status_info = document.getElementById("statusInfo");
    let deposit_submit_button = document.getElementById("depositSubmitBtn");

    // event listener for deposit form
    // this implements a custom submit method

    deposit_form.addEventListener("submit", (event) => {

        deposit_submit_button.disabled = true;

        event.preventDefault(); // Don't do the default submit action!

        let xhr = new XMLHttpRequest();
        let formData = new FormData(deposit_form);

        xhr.addEventListener("load", (event) => {

            status_ = event.target.status;
            response_ = event.target.responseText;

            if (status_ == 200 && response_ == "success") {
                status_info.innerText = "Success. Redirecting soon.";
                window.location.replace("/");
            }
            else {
                status_info.classList.add("errorText");
                status_info.innerText = "An error occured. Redirecting in 5 seconds...";
                window.setTimeout(() => { window.location.replace("/") }, 5000);
            }

        })

        xhr.addEventListener("error", (event) => {
            status_info.classList.add("errorText");
            status_info.innerText = "An error occured. Redirecting in 5 seconds...";
            window.setTimeout(() => { window.location.replace("/") }, 5000);
        })

        xhr.open("POST", "/api/deposit");
        xhr.send(formData);

    });
    
})