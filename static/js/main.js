document.addEventListener("DOMContentLoaded", () => {

    let dropDownMenuElement = document.getElementById("dropDownMenu");
    let dropDownMenuButtonElement = document.getElementById("dropDownMenuButton");

    if (dropDownMenuButtonElement != null) {

        dropDownMenuButtonElement.addEventListener("click", () => {

            if (dropDownMenuElement.classList.contains("dropDownVisible")) {
                dropDownMenuElement.classList.remove("dropDownVisible");
            }
            else {
                dropDownMenuElement.classList.add("dropDownVisible");
            }

        })

    }

})