{

    let statistics_dropdown_choices;
    let statistics_tables;

    let dropDownMenuActive = false;

    document.addEventListener("DOMContentLoaded", () => {

        // elements
        let statistics_dropdown_menu = document.getElementById("statisticsDropDownMenu");
        let statistics_dropdown_menu_button = document.getElementById("statisticsDropDownMenuButton");
        statistics_dropdown_choices = [...statistics_dropdown_menu.getElementsByClassName("sChoice")];
        statistics_tables = [...document.getElementsByClassName("statisticsTable")];

        statistics_dropdown_menu_button.addEventListener("click", () => {
            if (statistics_dropdown_menu.classList.contains("dropDownVisible")) {
                statistics_dropdown_menu.classList.remove("dropDownVisible");
            }
            else {
                statistics_dropdown_menu.classList.add("dropDownVisible");
            }
        })
        
        statistics_dropdown_choices.forEach(element => {

            element.addEventListener("click", () => {
                changeStatisticsChoice(element.innerText, element.dataset.statistics_div);
            })

        })

    })

    function changeStatisticsChoice(choice_name, div_id) {
        statistics_tables.forEach(element => {
            element.classList.add("nodisplay");
        })
        document.getElementById(div_id).classList.remove("nodisplay");
    }

}