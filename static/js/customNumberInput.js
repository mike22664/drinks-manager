document.addEventListener("DOMContentLoaded", () => {
    // get all customNumberInput Elements
    let custom_number_inputs = document.getElementsByClassName("customNumberInput");
    // Add Event Handler to the elements of the customNumberInputs
    [...custom_number_inputs].forEach(element => {
        // number input 
        let numberFieldElement = element.getElementsByClassName("customNumberInputField")[0];
        // minus button
        element.getElementsByClassName("customNumberInput-minus")[0].addEventListener("click", () => {
            alterCustomNumberField(numberFieldElement, -1)
        });
        // plus button
        element.getElementsByClassName("customNumberInput-plus")[0].addEventListener("click", () => {
            alterCustomNumberField(numberFieldElement, +1)
        });
    })
})

function alterCustomNumberField(numberFieldElement, n) {
    numberFieldElement.value = Math.min(
        Math.max(
            (parseInt(numberFieldElement.value) + n), numberFieldElement.min || Number.MIN_VALUE
        ),
        numberFieldElement.max || Number.MAX_VALUE
    );
}