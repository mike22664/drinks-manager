{

    // Define variables

    let username_input;
    let password_input;
    let submit_button;
    let username_display;
    let password_overlay;
    let pw_overlay_cancel;
    let userlist_buttons;
    let pinpad_buttons;


    // Add event listeners after DOM Content loaded

    document.addEventListener("DOMContentLoaded", () => {
        
        // elements

        username_input = document.getElementById("id_username");
        password_input = document.getElementById("id_password");
        submit_button = document.getElementById("submit_login");
        username_display = document.getElementById("usernameDisplay");
        password_overlay = document.getElementById("passwordOverlayContainer");
        pw_overlay_cancel = document.getElementById("pwoCancel");

        userlist_buttons = document.getElementsByClassName("userlistButton");
        pinpad_buttons = document.getElementsByClassName("pinpadBtn");

        // event listeners

        // [...<html-collection>] converts an html collection to an array

        [...userlist_buttons].forEach(element => {
            element.addEventListener("click", () => {
                set_username(element.dataset.username);
                show_password_overlay();
            })
        });

        [...pinpad_buttons].forEach(element => {
            element.addEventListener("click", () => {
                pinpad_press(element.dataset.btn);
            })
        })

        pw_overlay_cancel.addEventListener("click", () => {
            hide_password_overlay();
        });

    })


    function set_username(username) {

        username_input.value = username;
        username_display.innerText = username;

    }

    function show_password_overlay() {

        window.scrollTo(0, 0);
        password_overlay.classList.remove("nodisplay");
        document.body.classList.add("overflowHidden");
        //password_input.focus();

    }

    function hide_password_overlay() {

        password_overlay.classList.add("nodisplay");
        document.body.classList.remove("overflowHidden");

    }

    function pinpad_press(key) {
        if (key == "enter") {
            submit_button.click();
        }
        else if (key == "x") {
            password_input.value = "";
        }
        else {
            password_input.value += key;
        }
    }

}