document.addEventListener("DOMContentLoaded", () => {

    // elements

    let order_form = document.getElementById("orderForm");
    let status_info = document.getElementById("statusInfo");
    let order_submit_button = document.getElementById("orderSubmitBtn");

    // custom submit method

    order_form.addEventListener("submit", (event) => {

        order_submit_button.disabled = true;

        event.preventDefault(); // Don't do the default submit action!

        let xhr = new XMLHttpRequest();
        let formData = new FormData(order_form);

        xhr.addEventListener("load", (event) => {

            status_ = event.target.status;
            response_ = event.target.responseText;

            if (status_ == 200 && response_ == "success") {
                status_info.innerText = "Success.";
                window.location.replace("/");
            }
            else {
                status_info.classList.add("errorText");
                status_info.innerText = "An error occured.";
                window.setTimeout(() => { window.location.reload() }, 5000);
            }

        })

        xhr.addEventListener("error", (event) => {
            status_info.classList.add("errorText");
            status_info.innerText = "An error occured.";
            window.setTimeout(() => { window.location.reload() }, 5000);
        })

        xhr.open("POST", "/api/order-drink");
        xhr.send(formData);

    });

})